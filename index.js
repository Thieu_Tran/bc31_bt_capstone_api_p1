import {
  cancelTable,
  openDialog2,
  openTable,
  renderList,
  renderPurchase,
  renderTable,
} from "./controller.js";
import { dataServer } from "./dataServer.js";

let cartArr = [];
let dataJson = localStorage.getItem("ProductList");
if (dataJson != null) {
  cartArr = JSON.parse(dataJson);
}
// lưu dữ liệu vào localStorage
let dataToJson = (data) => {
  let listJson = JSON.stringify(data);
  localStorage.setItem("ProductList", listJson);
};

let cartOpen = () => {
  openTable(".cart_table");
};
window.cartOpen = cartOpen;
let cartCancel = () => {
  cancelTable(".cart_table");
};
window.cartCancel = cartCancel;

let renderServer = () => {
  dataServer
    .callDataServer()
    .then((res) => {
      dataToJson(cartArr);
      renderList(res.data);
      renderTable(cartArr);
    })
    .catch((err) => {});
};
renderServer();
// formSearch
let searchItem = () => {
  dataServer
    .callDataServer()
    .then((res) => {
      let dataArr = res.data;
      let arrFilter = [];
      let value = document.querySelector("#search_value").value;
      for (let index = 0; index < dataArr.length; index++) {
        if (dataArr[index].type == value) {
          arrFilter.push(dataArr[index]);
        }
      }
      if (arrFilter.length == 0) {
        arrFilter = dataArr;
      }
      renderList(arrFilter);
    })
    .catch((err) => {});
};
window.searchItem = searchItem;
// thêm vào giỏ hàng
let addToCart = (id) => {
  dataServer
    .callDataServer()
    .then((res) => {
      let index = cartArr.findIndex((item) => {
        return item.id == id;
      });

      if (cartArr.length == 0 || index == -1) {
        let indexRes = res.data.findIndex((resItem) => {
          return resItem.id == id;
        });
        let newSp = { ...res.data[indexRes], count: 1 };
        cartArr.push(newSp);
      } else {
        cartArr[index].count++;
      }

      dataToJson(cartArr);
      renderList(res.data);
      renderTable(cartArr);
    })
    .catch((err) => {});
};
window.addToCart = addToCart;
// tăng giảm số lượng
let upDownCount = (id, isValue) => {
  dataServer
    .callDataServer()
    .then((res) => {
      let index = cartArr.findIndex((item) => {
        return item.id == id;
      });
      if (isValue) {
        cartArr[index].count++;
      } else {
        cartArr[index].count--;
      }
      if (cartArr[index].count == 0) {
        cartArr.splice(index, 1);
      }
      dataToJson(cartArr);
      renderList(res.data);
      renderTable(cartArr);
    })
    .catch((err) => {});
};
window.upDownCount = upDownCount;
// xóa item
let removeToCart = (id) => {
  dataServer
    .callDataServer()
    .then((res) => {
      let index = cartArr.findIndex((item) => {
        return item.id == id;
      });
      if (index != -1) {
        cartArr.splice(index, 1);
      }
      dataToJson(cartArr);
      renderTable(cartArr);
      renderList(res.data);
    })
    .catch((err) => {});
};
window.removeToCart = removeToCart;
// clear cart
let clearCart = () => {
  dataServer
    .callDataServer()
    .then((res) => {
      cartArr = [];
      dataToJson(cartArr);
      renderTable(cartArr);
      renderList(res.data);
      cancelTable(".cart_table");
    })
    .catch((err) => {});
};
window.clearCart = clearCart;

let purchase = () => {
  renderPurchase(cartArr);
};
window.purchase = purchase;

let orderNow = () => {
  openDialog2();
  clearCart();
};
window.orderNow = orderNow;
